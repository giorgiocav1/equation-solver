def Is_an_equation(string_1):#Fonction qui verifie si le input est une equation
    res = 0
    for i in string_1:
        if i == "=":
            res += 1
        elif i.isdigit() == False and i not in ["x", "*", "+", "-", "/", " ", "(", ")"]:#.isdigit() verifie si quelque chose est un nombre
            return False            
    if res == 1:
        return True
    else:
        return False
def operation_type(string_1):
    res = ""
    for i in string_1:
        if i == "*":
            res = "multiplication"
        if i == "+":
            res = "addition"
        if i == "-":
            res = "substraction"
        if i == "/":
            res = "division"
    return res#Sert a rien
def split_string(string_1):
    res = []
    temp = ""
    for i in range(len(string_1)):
        temp = ""
        if string_1[i] == "(":
            num_temp = i+1
            while string_1[num_temp] != ")":
                temp += string_1[num_temp]
                """
                if string_1[num_temp] == "(":
                    num_temp = i+1
                    temp = ""
                else:
                """ 
                num_temp += 1
            res.append(temp)
    return res[0]#Sert a rien
def sides(string_1):#Fonction qui divise l'equation en deux parties 
    temp = 0
    left_side = ""
    right_side = ""
    while string_1[temp] != "=":
        left_side += string_1[temp] 
        temp += 1
    right_side = string_1[temp+2:]

    return left_side, right_side
string_equation = input("""Saisir une equation (l'equation doit avoir que une inconnue, nomme x):   
       """)
if Is_an_equation(string_equation) == False:
    print("Pas correct")
left_side, right_side = sides(string_equation)